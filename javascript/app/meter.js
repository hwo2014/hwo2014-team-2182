var fs = require('fs');
var _ = require('lodash');

var fileName = "data/speeds_" + Date.now() + '.txt';

var meter = {

  getSpeed: function(carPositions, track) {

    //diff seems to want something to begin with
    var initialValue = {
        gameTick: 0,
        id: { name: 'Kirjokansi' },
        angle: 0,
        piecePosition: {
          pieceIndex: 0,
          inPieceDistance: 0.0,
          lane: { startLaneIndex: 0, endLaneIndex: 0 },
          lap: 0
      }
    };

    return carPositions
      .diff(initialValue, function(previousPosition, currentPosition) {
        var currentIndex = currentPosition.piecePosition.pieceIndex;
        var prevIndex = previousPosition.piecePosition.pieceIndex;
        var diff = 0;

        // when piece changes, we need to calculate what was left in previous piece.
        if(currentIndex !== prevIndex) {
          var currentLane = currentPosition.piecePosition.lane.endLaneIndex;
          var currentPiece = track.pieces[currentIndex];
          var lastPieceLength =
            meter.getLaneLength(
                track.pieces[parseInt(prevIndex)],
                currentLane,
               track.lanes
            );
          var prevPieceDiff =
            lastPieceLength - previousPosition.piecePosition.inPieceDistance;
          currentPosition.diff = prevPieceDiff + currentPosition.piecePosition.inPieceDistance;
        } else {
          currentPosition.diff = currentPosition.piecePosition.inPieceDistance - previousPosition.piecePosition.inPieceDistance;
        }
        //return { 'tick': currentPosition.gameTick, 'car': currentPosition.id.name, 'diff' : diff};
        return currentPosition;
      });
  },

  getAcceleration: function(speeds) {
    return speeds
      .diff({'diff' : 0}, function(v1, v2) {
        v2.acc = v2.diff - v1.diff; //(v2 - v1)/tick
        return v2;
      });
  },

  getMinMaxAcceleration: function(accelerations) {
    return accelerations
      .diff({'gameTick': 0, 'acc' : 0, 'maxAcc': 0}, function(acc1, acc2) {
        // TODO: acceleration starts quite high values... need to figure out what to do.
        acc2.maxAcc =
          acc2.acc > acc1.maxAcc && acc2.gameTick > 22 ? acc2.acc : acc1.maxAcc;
        return acc2;
      })
      .diff({'gameTick': 0, 'acc' : 0, 'minAcc': 0}, function(acc1, acc2) {
        acc2.minAcc =
          acc2.acc < acc1.minAcc && acc2.gameTick > 22 ? acc2.acc : acc1.minAcc;
        return acc2;
      });
  },

  getLaneLength: function(piece, laneIndex, lanes) {
    if(piece.length != null) {
      return piece.length;
    } else {
      var sign = piece.angle > 0 ? -1 : 1;
      return Math.abs(piece.angle) / 180 * Math.PI * (piece.radius + sign * lanes[laneIndex].distanceFromCenter);
    }
  },

   getMeasures: function(carPositions, track) {
    return meter.getMinMaxAcceleration(
      meter.getAcceleration(
        meter.getSpeed(carPositions, track)
      )
    );
  },

  log: function(stream){
    stream.onValue(function(data) {
      if(process.env.LOG_TO_FILE) {
        fs.appendFile(fileName, JSON.stringify(data)+",\n" , function (err) {
          if (err) throw err;
        });
      }
    });
  }

};

module.exports = meter;