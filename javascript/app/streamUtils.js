var Bacon = require('baconjs').Bacon;

// 1. Takes a function and a variable amount of parameters: fromCallbackKeepAlive(fn, arg1, arg2...)
// 2. Applies the parameters to the function
// 3. Creates a stream based on the callback of the function - callback parameters are passed to the stream as values
function fromCallbackKeepAlive(fn){
  var args = Array.prototype.slice.call(arguments, 1); //extract the arguments, except the function

  return Bacon.fromBinder(
    function(sink){
      //push the function callback arguments to the stream
      function callback(){ 
        sink.apply(this, arguments);
      }
      fn.apply(this, args.concat(callback));
      
      //for now, empty unsubscribe function
      return function(){}; 
    }
  );
}

exports.fromCallbackKeepAlive = fromCallbackKeepAlive;