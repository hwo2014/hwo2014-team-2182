var _ = require('lodash');
var JSONStream = require('JSONStream');

var car = {
  throttle: function (track, positions, name){
    var pieces = track.pieces;
    var currentPosition = this.getCarPosition(positions, name);

    var index = currentPosition.piecePosition.pieceIndex;
    var enteringBend = pieces[(index + 2) % pieces.length].length === undefined;

    return car._throttleMessage(enteringBend ? 0.5 : 1);
  },

  getCarPosition: function(positions, name) {
    return _.find(positions, { 'id': {'name': name}});
  },

 getDecisions: function(measures, serverData, track, futureTicks) {
    //var futurePieces = track.pieces[serverData.piecePosition.pieceIndex + 2 % track.pieces.length];
    //jotain...
    //futureTicks.push(car.throttle(track, ));
  },

  _throttleMessage: function(data) {
    return {
      msgType: "throttle",
      data: data
    };
  }
};

module.exports = car;