var Bacon = require('baconjs').Bacon;
var _ = require('lodash');
var streamUtils = require('./app/streamUtils');
var net = require('net');
var JSONStream = require('JSONStream');
var Car = require('./app/car');
var Meter = require('./app/meter');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

var client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
}

function filterOnType(type, data){
  return data.msgType === type;
}

//Keep Dry; Put somewhere else - car.js has now duplicate code
function filterOwn(data){
  return _.find(data, function(p){
    return  p.id.name === botName;
  });
}

var jsonStream = client.pipe(JSONStream.parse());
var track = [];
var messageStream = streamUtils.fromCallbackKeepAlive(jsonStream.on.bind(jsonStream), 'data');

var filterOnPositions = _.partial(filterOnType, "carPositions");

var message = '';
var ownPosition = messageStream
  .filter(filterOnPositions)
  .map(function(tickData) {
      var data = tickData.data;
      carData = _.find(data, { 'id': {'name': botName}});
      carData.gameTick = tickData.gameTick;
      carData.lastMsg = message;
      return carData
    }
  );

messageStream.onValue(function(data){
  var futureTicks = new Bacon.Bus();
  var carMeasures = null;

  if (data.msgType === 'carPositions') {
    Car.getDecisions(carMeasures, data.data, track, futureTicks);
    message = Car.throttle(track, data.data, botName);
    send(message);
  } else {
    if (data.msgType === 'join') {
      console.log('Joined');
    } else if (data.msgType === 'gameStart') {
      console.log('Race started: ', Date.now());
    } else if (data.msgType === 'gameInit') {
      track = data.data.race.track;

      carMeasures = Meter.getMeasures(ownPosition, track);
      Meter.log(carMeasures);

      console.log('Race inited: ', track.pieces.length);
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
      console.log('Results:\n');
      console.log(data.data);
    } else if (data.msgType === 'dnf'){
      console.log('DNF: ', data.data.reason);
    } else if (data.msgType === 'finish'){
      console.log('Finished: ', data.data);
    } else {
      //console.log(data.msgType);
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
